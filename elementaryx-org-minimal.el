(defun org-babel-lob-ingest-from-emacslobpath (filename)
  "Load a file from the directories specified in EMACSLOBPATH (the EMACS Library Of Babel PATH)."
  (let ((org-babel-lob-dirs (parse-colon-path (getenv "EMACSLOBPATH"))))
    (dolist (dir org-babel-lob-dirs)
      (let ((file-path (expand-file-name filename dir)))
        (when (file-exists-p file-path)
          (org-babel-lob-ingest file-path)
          (message "Library Of Babel %s loaded from %s" filename dir))))))

(use-package org
  :defer t
  :config
  (add-to-list 'org-modules 'org-id t) ;; use org-id for handling links
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((scheme . t)
     (C . t)
     (emacs-lisp . t)
     (python . t)
     (R . t)
     (shell . t)
     (latex . t)
     (dot . t))))

(use-package org-src
  :defer t
  :config
  (add-to-list 'org-src-lang-modes '("c++" . c++))
  :custom
  ;; https://orgmode.org/manual/Editing-Source-Code.html . When
  ;; org-src-preserve-indentation is non-nil, source code is aligned
  ;; with the leftmost column. No lines are modified during export or
  ;; tangling, very useful for white-space sensitive languages
  (org-src-preserve-indentation t))

(use-package ob-core
  :defer t
  :custom
  (org-confirm-babel-evaluate nil))

(use-package ob-python
  :defer t
  :custom
  (org-babel-python-command "python3"))

(provide 'elementaryx-org-minimal)
